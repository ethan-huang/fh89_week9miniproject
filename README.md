# fh89_week9miniproject

## Goal

This project demonstrates how to connect to a Language Model (LLM) using Python and Streamlit. Streamlit provides an interactive and intuitive way to showcase machine learning models and applications. By connecting Streamlit to an LLM, users can interactively generate text based on input prompts.

## Project URL

- https://ids721mini9-gxv9caay2mfozjs2lgjnfk.streamlit.app/

## Project set up

- download streamlit by `pip3 install streamlit`

- test streamlit by `streamlit hello`

- create a requirements.txt for all of the dependencies needed for LLM model.

- import the requirements by `pip3 install -r requirements.txt`

# Local test

- run `streamlit run <app.py>`, and it will show on the localhost:8501

# Deploy

- Go to streamlit webpage to sign in

- Go to https://share.streamlit.io/ and click new app / using existing repo (first we need to upload to the github)

# Result

- ![1](./img/streamlit.png)

- ![2](./img/deploy.png)

- ![3](./img/generate.png)
